import numpy as np
import math 

def Hamiltonian(Lattice,x,y):
	J=1.0
	H=0.0
	Lx=len(Lattice)
	Ly=len(Lattice[1,:])

	for i in range(2,-3,-1):
		for j in range(2,-3,-1):
			H += -J*Lattice[x][y]*Lattice[(x+i)%Lx][(x+j)%Ly]

	return H

def MC_Move(Lattice,x,y,kbT):
	E1=Hamiltonian(Lattice,x,y)
	Lattice[x][y]= -Lattice[x][y]
	E2=Hamiltonian(Lattice,x,y)
	dE=E2-E1

	if(dE>0):
		chi=np.random.random()
		if(math.exp(-dE/kbT)>chi):
			#Then return lattice to original state,i.e reject move
			Lattice[x][y] = -Lattice[x][y]
		
	#otherwise we do nothing ...i.e accept the change.

def MC_Sweep(Lattice,kbT):
	Lx=len(Lattice)
        Ly=len(Lattice[1,:])
		
	NSteps=Lx*Ly*1000
	for t in range(NSteps):
		x=np.random.randint(0,Lx)
		y=np.random.randint(0,Ly)
		MC_Move(Lattice,x,y,kbT)
	

	
