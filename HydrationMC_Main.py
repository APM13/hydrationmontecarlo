import numpy as np
import math
import functions as funcs
import matplotlib.pyplot as plt



nx=20
ny=20

lattice=np.random.choice([1,-1],(nx,ny))

for T in range(100,-20,-30):
	kbT=(float(T)/300)
	funcs.MC_Sweep(lattice,kbT)
	print T

	plt.pcolor(lattice)
	plt.savefig("test_"+str(T)+".pdf")




